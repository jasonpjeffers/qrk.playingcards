// Lab 2: Playing Cards
// Part1: Quinton Kingswan
// Part2: Jason Jeffers

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit { Hearts = 1, Diamonds, Spades, Clubs };
enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };

struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card c1) 
{
	switch (c1.Rank)
	{
	case Two: cout << "The Two"; break;
	case Three: cout << "The Three"; break;
	case Four: cout << "The Four"; break;
	case Five: cout << "The Five"; break;
	case Six: cout << "The Six"; break;
	case Seven: cout << "The Seven"; break;
	case Eight: cout << "The Eight"; break;
	case Nine: cout << "The Nine"; break;
	case Ten: cout << "The Ten"; break;
	case Jack: cout << "The Jack"; break;
	case Queen: cout << "The Queen"; break;
	case King: cout << "The King"; break;
	case Ace: cout << "The Ace"; break;
	};
	
	switch (c1.Suit)
	{
	case Hearts: cout << " of Hearts\n"; break;
	case Diamonds: cout << " of Diamonds\n"; break;
	case Spades: cout << " of Spades\n"; break;
	case Clubs: cout << " of Clubs\n"; break;
	};
}

Card HighCard(Card c1, Card c2) 
{
	if (c1.Rank > c2.Rank)
	{
		return c1;
	}
	else
	{
		return c2;
	}
}

int main()
{
	Card c1;
	Card c2;
	c1.Rank = Five;
	c1.Suit = Hearts;
	c2.Rank = Ace;
	c2.Suit = Spades;
	PrintCard(c1);
	PrintCard(c2);
	cout << "Which card is higher: \n";
	PrintCard(HighCard(c1, c2));




	(void)_getch();
	return 0;
}
